**Table of Contents**
<!-- @import "[TOC]" {cmd="toc" depthFrom=1 depthTo=6 orderedList=false} -->

<!-- code_chunk_output -->

* [The Binary Number System](#the-binary-number-system)
	* [The Concept of a Number System](#the-concept-of-a-number-system)
	* [Decimal to Binary](#decimal-to-binary)
		* [To write a number in whatever number system:](#to-write-a-number-in-whatever-number-system)
	* [Binary to Decimal](#binary-to-decimal)
	* [Addition](#addition)
	* [Subtraction](#subtraction)
	* [multiplication](#multiplication)
	* [division](#division)
* [The Octal Number System](#the-octal-number-system)
	* [Decimal to Octal](#decimal-to-octal)
	* [Octal to Decimal](#octal-to-decimal)
	* [Addition](#addition-1)
	* [Subtraction](#subtraction-1)
	* [Multiplication](#multiplication-1)
	* [Division](#division-1)
* [Binary and Octal](#binary-and-octal)
	* [Octal to Binary](#octal-to-binary)
	* [Binary to Octal](#binary-to-octal)

<!-- /code_chunk_output -->

# The Binary Number System
**0 to 9 and combinations of however much of these "symbols"**, this more or less describes our number system `The decimal system or` **Base-10**.
Other concepts of numbers are extremely useful in their respective fields, like binary(**base-2**) to conceptualize a switch and **base-60** to conceptualize the math ancient Egyptians used for Astronomy and Geometry. In the modern world the Binary number system has many uses.
<br>

**The Binary number system is used for**
+ Machine code (what all programming languages eventually get compiled to)
+ Boolean logic can be easily traced with 1s and 0s since the binary number system represents a switch (on/off)
+ Floating Points, ASCII Codes, and Storage of Analog Data

## The Concept of a Number System
Number systems are just variations of the same number on a different table, for example the number **426** consists of the 3 parts the `Hundreds`, the `Tens` and the `Ones` which map onto`4-hundreds` , `2-tens`, and `6-ones`.

|**Hundreds (10<sup>2 </sup>)**|**Tens(10<sup>1 </sup>)** | **Ones (10<sup>0 </sup>)**
|:-:|:-:|:-:|
| **4**| **2**| **6**|

**Note**
Throughout the lesson numbers will have their base written as a subscript next to the number.

**Examples:**

+ **426<sub>10</sub>** is 426 in base 10 `Decimal`
+ **101<sub>2</sub>** is 5 in base 2 `Binary`
+ **13<sub>8</sub>** is 11 in base 8 `Octal`

## Decimal to Binary
The Base-2 or more commonly Binary number system is just another table to represent  the concept of a number only difference is:

Instead of the scale going
<br>
**10<sup>0</sup> (`ones`), 10<sup>1</sup> (`tens`), 10<sup>2</sup>(`hundreds`) ....**
<br>
It goes
<br>
**2<sup>0</sup> (`ones`), 2<sup>1</sup> (`twos`), 2<sup>2</sup>(`fours`) ....**
<br>

### To write a number in whatever number system:
+ The cells in the Base-`n` number system table can be filled with numbers from 0 to `n`-1
+ Fill the number system's table with the greatest number first and then the next greatest number and so on

**example**

*Write the number 47 in Binary(Base-2)*

**Step 1**
Write out the number system table with the greatest number less than 47 as your leftmost column with all others are at 0

|**Thirty-Twos (2<sup>5 </sup>)**|**Sixteens (2<sup>4 </sup>)**|**Eights (2<sup>3 </sup>)**|**Fours (2<sup>2 </sup>)**|**Twos(2<sup>1 </sup>)** | **Ones (2<sup>0 </sup>)**|
|:-:|:-:|:-:|:-:|:-:|:-:|
| **0**  | **0**  | **0**  | **0**  | **0**  | **0**  |

**Step 2** Fill out the table with the greatest numbers that go into 47 from greatest down to least, since it is based-2 we can have either 0 or 1 (`n`-1) in our cells

|**Thirty-Twos (2<sup>5 </sup>)**|**Sixteens (2<sup>4 </sup>)**|**Eights (2<sup>3 </sup>)**|**Fours (2<sup>2 </sup>)**|**Twos(2<sup>1 </sup>)** | **Ones (2<sup>0 </sup>)**|
|:-:|:-:|:-:|:-:|:-:|:-:|
| **1**  | **0**  | **0**  | **0**  | **0**  | **0**  |

|**Thirty-Twos (2<sup>5 </sup>)**|**Sixteens (2<sup>4 </sup>)**|**Eights (2<sup>3 </sup>)**|**Fours (2<sup>2 </sup>)**|**Twos(2<sup>1 </sup>)** | **Ones (2<sup>0 </sup>)**|
|:-:|:-:|:-:|:-:|:-:|:-:|
| **1**  | **0**  | **1**  | **0**  | **0**  | **0**  |

|**Thirty-Twos (2<sup>5 </sup>)**|**Sixteens (2<sup>4 </sup>)**|**Eights (2<sup>3 </sup>)**|**Fours (2<sup>2 </sup>)**|**Twos(2<sup>1 </sup>)** | **Ones (2<sup>0 </sup>)**|
|:-:|:-:|:-:|:-:|:-:|:-:|
| **1**  | **0**  | **1**  | **1**  | **0**  | **0**  |

|**Thirty-Twos (2<sup>5 </sup>)**|**Sixteens (2<sup>4 </sup>)**|**Eights (2<sup>3 </sup>)**|**Fours (2<sup>2 </sup>)**|**Twos(2<sup>1 </sup>)** | **Ones (2<sup>0 </sup>)**|
|:-:|:-:|:-:|:-:|:-:|:-:|
| **1**  | **0**  | **1**  | **1**  | **1**  | **0**  |

|**Thirty-Twos (2<sup>5 </sup>)**|**Sixteens (2<sup>4 </sup>)**|**Eights (2<sup>3 </sup>)**|**Fours (2<sup>2 </sup>)**|**Twos(2<sup>1 </sup>)** | **Ones (2<sup>0 </sup>)**|
|:-:|:-:|:-:|:-:|:-:|:-:|
| **1**  | **0**  |  **1** | **1**  | **1**  | **1**  |

<br>

Once the table is filled, tada!!! The number is converted to binary

**47<sub>10</sub>** converts to **101111<sub>2</sub>** in binary
## Binary to Decimal
Similarly, converting backwards from binary to decimal is just trivial use of a table.

**example**
 *Write the number 101101101<sub>2</sub> in Decimal(Base-10)*

**Step 1** insert the number into it's respective number system table

**256 (2<sup>8 </sup>)**|**128 (2<sup>7 </sup>)**|**64 (2<sup>6 </sup>)**|**32 (2<sup>5 </sup>)**|**1s (2<sup>4 </sup>)**|**8 (2<sup>3 </sup>)**|**4 (2<sup>2 </sup>)**|**2 (2<sup>1 </sup>)** | **1 (2<sup>0 </sup>)**|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
| **1**  | **0**  |  **1** | **1**  | **0**  | **1**  | **1**  | **0**  | **1**  |

**Step 2** add the corresponding values multiplied by their representations

```math
256(1) + 128(0) + 64(1) + 32(1) + 16(0) + 8(1) + 4(1) + 2(0) + 1(0) = 365
```
and there you have it 101101101<sub>2</sub> converts to 365<sub>10</sub>

## Addition
Adding in binary is very simple once carries are taken care of

**example**
 *add the numbers 11011100<sub>2</sub> and 10101101<sub>2</sub>*

**Step 1** Align the numbers as you would in normal long addition

```math
\begin {gathered}
\space\space\space\space11011100 \\
+\space10101101\\
------
\end {gathered}
```

**Step 2** Start adding from right to left but remember the following
+ 0 + 0 is 0<sub>10</sub> which is 0<sub>2</sub>
+ 1 + 0 is 1<sub>10</sub> which is 1<sub>2</sub>
+ 1 + 1 is 2<sub>10</sub> which is 01<sub>2</sub>
+ 1 + 1 + 1 is 3<sub>10</sub> which is 11<sub>2</sub>

```math
\begin {aligned}
11111\space\space0\space\space\\
\space\space\space\space11011100 \\
+\space10101101\\
110001001
\end {aligned}
```
Notice how the columns that add to more than 1 are converted to binary then carried (as stated in the previous steps)

## Subtraction
Just like addition, subtraction is very trivial in binary.

**example**
 subtract 1101<sub>2</sub> from 10101<sub>2</sub>

**Step 1** Align the numbers as you would in normal long subtraction
```math
\begin{aligned}
10101\\
-\space1101
\end{aligned}
```

**Step 2** subtract like you normally would but remember the following
+ 1 - 1 = 0
+ 1 - 0 = 1
+ 0 - 1 = carry from nearest one transforming it to 0 and the current 1 to 10 (2<sub>10</sub>)

```math
\begin{aligned}
10101\\
-\space1101\\
\space000\\
\end{aligned}
```
Notice how the carrying is going to happen now
```math
\begin{aligned}
2\space\space\space\space\space\space\\
00101\\
-\space1101\\
\space000\\
\end{aligned}
```
remember that the 2 is 10 in binary
```math
\begin{aligned}
2\space\space\space\space\space\space\\
00101\\
-\space1101\\
\space01000\\
\end{aligned}
```
## multiplication
Multiplication is by far the most trivial of operations in binary, it boils down to organization because you either write the number(multiply by 1) or don't(multiply by 0)

Multiple by one

```math
\begin{aligned}
10101\\
*\space101\\
10101
\end{aligned}
```

Multiple by 0 after appending a zero to the answer

```math
\begin{aligned}
10101\\
*\space101\\
10101\\
000000
\end{aligned}
```

Multiple by 1 after appending a 2 zeros to the answer

```math
\begin{aligned}
10101\\
*\space101\\
10101\\
000000\\
+\space1010100
\end{aligned}
```

Finally add

```math
\begin{aligned}
10101\\
*\space101\\
10101\\
000000\\
+\space1010100\\
1101001
\end{aligned}
```

## division
Dividing binary is also a trivial operation to get done

**Step 1** Write the numbers in long division form

```math
\begin {gathered}
110\overline{)10111}
\end {gathered}
```

**Step 2** Divide just like you normally would but interpret the groups of bits as numbers

does 10<sub>2</sub> go into 11<sub>2</sub> ?
```math
\begin {aligned}
0\space\space\space\space\space\space\space\space\\
110\overline{)10111}\\
-\space00\space\space\space\space\space\space
\end {aligned}
```

Bring down a couple of bits

```math
\begin {aligned}
0\space\space\space\space\space\space\space\space\\
110\overline{)10111}\\
-\space00\space\space\space\space\space\space\\
1011\space\space
\end {aligned}
```
Does 1011 <sub>2</sub> go into 110 <sub>2</sub> ?
Bring down the last bit and divide again
```math
\begin {aligned}
01\space\space\space\space\space\space\\
110\overline{)10111}\\
-00\space\space\space\space\space\space\\
1011\space\space\\
-110\space\space\\
1011
\end {aligned}
```

```math
\begin {aligned}
011\space\space\space\space\\
110\overline{)10111}\\
-00\space\space\space\space\space\space\\
1011\space\space\\
-110\space\space\\
1011\\
-110\\
101 \space\space R
\end {aligned}
```
And we are left with a remainder

# The Octal Number System
The octal number system is another extremely useful number system in the modern world and it has a lot of uses.

+ Binary files are often represented in octal because every 3 binary bits convert to one octal number
+ Widely used in 12, 24, and 36 bit word systems
+ Commonly used in modern programming languages however hexadecimal is more widespread
+ Used in digital displays
+ Used to distinguish different aircraft on a radar

## Decimal to Octal

**Example**
Convert the number 129<sub>10</sub> to octal

Just like binary a table is extremely useful to convert from decimal to octal, however the cells go from 0 to 7 not just 0 and 1 and the columns are exponents of 8 not 2

**Step 1** start with a zero table

|**Sixty-fours (8<sup>2 </sup>)**|**Eights(8<sup>1 </sup>)** | **Ones (8<sup>0 </sup>)**|
|:-:|:-:|:-:|
| **0**  | **0**  | **0**|

**Step 2** Fill in with the most cell you can with its most multiple and so on

|**Sixty-fours (8<sup>2 </sup>)**|**Eights(8<sup>1 </sup>)** | **Ones (8<sup>0 </sup>)**|
|:-:|:-:|:-:|
| **2**  | **0**  | **1**|

that translates to 64(2) + 0(8) + 1(1)

## Octal to Decimal
Converting from octal to decimal is generalized in [To write a number in whatever number system](#to-write-a-number-in-whatever-number-system) but the exact operation is as follows

**Example**
*Write the number 1342<sub>8</sub> in decimal

**Step 1** Write out the table of the octal system and fill in the number

|**128s (8<sup>3 </sup>)**|**64s (8<sup>2 </sup>)**|**8s (8<sup>1 </sup>)** | **1s (8<sup>0 </sup>)**|
|:-:|:-:|:-:|:-:|
| **1** | **3**  | **4**  | **2**|

that translates to 64(2) + 0(8) + 1(1)

**Step 2** Add the cells multiplied by their corresponding column values

1(8<sup>3</sup>) + 3(8<sup>2</sup>) + 4(8<sup>1</sup>) + 2(8<sup>0</sup>) = (128\*1) + (3\*64) + (4\*8) + (2\*1) = 354

## Addition
Addition in octal is the same as in binary only carries aren't 2s but 8s due to octal being base-8

**Example**
 *What is the sum of 732<sub>8</sub> and 57<sub>8</sub> ?*

**Step 1** Align the numbers vertically like you learned in the first grade

```math
\begin{aligned}
732\\
+\space\space\space57\\
\end{aligned}
```

**Step 2** Remember that the maximum single digit for base-n is n-1 for example base-10 is 9 hence for base-8 it is 7, carry accordingly

```math
\begin{aligned}
1\space\space\\
732\\
+\space\space\space57\\
1
\end{aligned}
```
Note that 8 is written as 10<sub>8</sub> so 9 is written as 11<sub>8</sub> thus the carrying

```math
\begin{aligned}
11\space\space\\
732\\
+\space\space\space57\\
1011
\end{aligned}
```
the carrying goes on until the last digit then is written out as a sum of the last digit plus the accumulated carrying

## Subtraction
Similarly subtracting is based on just recognizing that borrowing is going to be by eights not by twos like in binary or in 10s like in decimal

**Example**
 *What is the difference of 732<sub>8</sub> and 57<sub>8</sub> ?*

**Step 1** Align the numbers like your sibling taught you when you were zoned out as a child

```math
\begin{aligned}
732\\
-\space\space\space57\\
\end{aligned}
```

**Step 2** Start negating, if not possible just carry by 8s

```math
\begin{aligned}
72\space\color{#909090}{12}\\
-\space\space\space5\space\space\space7\\
\end{aligned}
```

Note we borrowed one from 3 and gave 2 an 8 which converts to 12<sub>8</sub>

```math
\begin{aligned}
72\space\color{#909090}{12}\\
-\space\space\space5\space\space\space7\\
3
\end{aligned}
```

```math
\begin{aligned}
6\space\space\color{#909090}{12}\color{#000000}\space12\\
-\space\space\space\space\space\space\space5\space\space\space7\\
6\space\space\space\space5\space\space\space3
\end{aligned}
```
And finally the answer is 653<sub>8</sub>

## Multiplication
Multiplication is another no sweat operation to do with octat numbers due to its need of only one simple step more than the normal route multiplication of numbers

**Example**
 Multiply the number 36<sub>8</sub> by 21<sub>8</sub>

**Step 1** Write out the numbers just like we did 1024<sub>8</sub> times previously

```math
\begin{aligned}
36\\
*\space\space\space21
\end{aligned}
```

**Step 2** multiply like you normally would but when the multiplication exceeds 7 convert to octal then carry

```math
\begin{aligned}
36\\
*\space\space\space21\\
36\\
0
\end{aligned}
```
Note here 2 * 6 is 12 which far exceeds our digits of octal, 12 converts to 14 in octal that is 8(1) + 1(4)

```math
\begin{aligned}
1\space\space\\
36\\
*\space\space\space21\\
36\\
740
\end{aligned}
```

Finally add

```math
\begin{aligned}
1\space\space\\
36\\
*\space\space\space21\\
36\\
+ \space\space740\\
776
\end{aligned}
```

## Division
As you have seen before division doesn't have any fast process it is just the matter of asking whether a number goes into another

**Example**
 solve the given long division diagram
```math
\begin{aligned}
14\overline{)555}\\
\end{aligned}
```

```math
\begin{aligned}
3\space\space\space\space\\
14\overline{)555}\\
-44\space\space\\
115

\end{aligned}
```

```math
\begin{aligned}
36\space\space\\
14\overline{)555}\\
-44\space\space\\
115\\
-110\\
005 R
\end{aligned}
```
So 5 is the remainder (Mod) of 555<sub>8</sub> / 14<sub>8</sub> with the answer of 36<sub>8</sub>

# Binary and Octal
Like Binary and Hexadecimal, Binary and octal are relevant together due to Octal correspondence to 3 bits of byte
## Octal to Binary
Given a binary file, reading it in groups of 3 bits would be easier than a full binary byte therefore octal is the answer. In 3 bits a maximum of the number 7 can be written and conveniently in octal that is one digit.

**Example**
 Given the number 25<sub>8</sub>. convert to binary.

**Solution**
split the number into digits then convert each
2<sub>8</sub> into binary would look like

|**Fours (2<sup>2 </sup>)**|**Twos(2<sup>1 </sup>)** | **Ones (2<sup>0 </sup>)**|
|:-:|:-:|:-:|
| **0**  | **1**  | **0**  |

5<sub>8</sub> into binary would look like

|**Fours (2<sup>2 </sup>)**|**Twos(2<sup>1 </sup>)** | **Ones (2<sup>0 </sup>)**|
|:-:|:-:|:-:|
| **1**  | **0**  | **1**  |

and now the number is simple the two tri-bits concatenated together 010 101 is 25 in octal

## Binary to Octal
Given the byte **011 110** convert it to octal

**Solution** Split the byte and convert each to a single octal digit

|**Fours (2<sup>2 </sup>)**|**Twos(2<sup>1 </sup>)** | **Ones (2<sup>0 </sup>)**|
|:-:|:-:|:-:|
| **0**  | **1**  | **1**  |

Now this is 0(4) + 1(2) + 1(1) which is 3 and that is also 3<sub>8</sub>

|**Fours (2<sup>2 </sup>)**|**Twos(2<sup>1 </sup>)** | **Ones (2<sup>0 </sup>)**|
|:-:|:-:|:-:|
| **1**  | **1**  | **0**  |

Now this is 1(4) + 1(2) + 0(1) which is 6 and that is also 6<sub>8</sub>

Finally concatenating all that is 36<sub>8</sub>